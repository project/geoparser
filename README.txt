A module to provide a common interface for geoparsing services such as Yahoo!'s Placemaker and various CCK fields.
Geoparsing?

Geoparsing is the process by which structured geographic locations and their identifiers (eg placenames, lat-long coordinates) are extracted from unstructured content, such as a blog post. Different techniques are used by various providers of geoparsing services, but broadly speaking the process is to identify placenames within the text, identify the context in which the placename exists within the text and then lookup the placename in a database of placenames.

This module passes a node's content to a geoparsing service (provided by a geoparser service module.) and saving the structured information returned by the service into cck fields. The module can store coordinate, placename and place id information and provides a per-node-type configuration. It also provides functionality to bulk geoparse nodes and an action that can be triggered on node insert or update.
Installing and Configuration

   1. Install this module
   2. Install one or mode geoparser service modules
   3. Go to admin/settings/geoparser and set geoparsing service module. Select the cck fields for each node type that you want to store lat-long, placenames, and ids (these are the unique ids used by the selected geoparsing service). Click save.

Usage

Regular usage: Any node-types that are configured to store geoparsed data are provided with a new menu tab (ie a MENU_LOCAL_TASK). Clicking this sends the node's content to your selected geoparsing service and displays a list of places returned by the service. The user can then select which of the places they wish to save information for.

Triggers: Go to admin/build/trigger/node to automatically trigger geoparsing a node after saving a new post or saving an updated post. The action is called 'geoparse a node'

Bulk: Go to admin/geoparse_bulk, select the node types to geoparse and click the 'Geoparse' button. This sets off a batch process that geocodes each node of the specified type(s). Places that meet the value 'Min value for default select:' value are automatically savd in this process.
Geoparser service modules

The geoparser module relies on other modules to do the job of sending content out to geoparsing services. A module can offer this service by implementing hook_geoparse_simple. The following modules provide this hook:

    * Yahoo! Placemaker
    * Edina Unlock

